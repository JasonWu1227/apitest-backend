import jmespath
from loguru import logger
from tep.client import request


def test(faker_ch, login, env_vars):
    fake = faker_ch
    logger.info("新增")
    nickname = fake.name()
    phone = fake.phone_number()
    response = request(
        "post",
        url=env_vars.domain + "/api/users",
        headers=login.jwt_headers,
        json={
            "nickname": nickname, "phone": phone
        }
    )
    assert response.status_code < 400
    user_id = jmespath.search("id", response.json())
    created_at = jmespath.search("createdAt", response.json())
    updated_at = jmespath.search("updatedAt", response.json())

    logger.info("查询")
    response = request(
        "get",
        url=env_vars.domain + "/api/users",
        headers=login.jwt_headers,
        params={
            "page": 1,
            "perPage": 10,
            "keyword": nickname
        }
    )
    assert response.status_code < 400

    logger.info("修改")
    nickname_new = fake.name()
    phone_new = fake.phone_number()
    response = request(
        "put",
        url=env_vars.domain + f"/api/users/{user_id}",
        headers=login.jwt_headers,
        json={
            "id": user_id, "createdAt": created_at, "updatedAt": updated_at,
            "phone": phone_new, "nickname": nickname_new
        }
    )
    assert response.status_code < 400
    logger.info(f"用户姓名手机 {nickname} {phone} 修改后 {nickname_new} {phone_new}")

    logger.info("删除")
    response = request(
        "delete",
        url=env_vars.domain + f"/api/users/{user_id}",
        headers=login.jwt_headers
    )
    assert response.status_code < 400