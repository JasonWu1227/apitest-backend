#!/usr/bin/python
# encoding=utf-8


from tep.dao import mysql_engine
from tep.fixture import *


@pytest.fixture(scope="session")
def env_vars(config):
    class Clazz(TepVars):
        env = config["env"]
        mapping = {'fat': {'domain': 'https://fat.com'}}
        domain = mapping[env]["domain"]

    return Clazz()
