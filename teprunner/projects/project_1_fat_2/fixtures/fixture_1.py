from tep.client import request
from tep.fixture import *


def _jwt_headers(token):
    return {"Content-Type": "application/json", "authorization": f"Bearer {token}"}


@pytest.fixture(scope="session")
def login(env_vars):
    # Code your login
    logger.info("Administrator login")
    response = request(
        "post",
        url=env_vars.domain + "/api/users/login",
        headers={"Content-Type": "application/json"},
        json={
            "username": "admin",
            "password": "qa123456",
        }
    )
    assert response.status_code < 400
    response_token = jmespath.search("token", response.json())
    super_admin_id = jmespath.search("user.id", response.json())

    class Clazz:
        token = response_token
        jwt_headers = _jwt_headers(response_token)
        admin_id = super_admin_id

    return Clazz